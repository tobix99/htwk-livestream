#my api key: AE72BA53

import pafy
import numpy as np
import cv2
import threading
import paho.mqtt.client as mqtt

#import matplotlib.pyplot as plt
#import cvlib as cv
from cvlib.object_detection import draw_bbox

import base64
import json
import codecs

import time

import pickle

import zlib

client_hetzner = mqtt.Client()
client_htwk = mqtt.Client()

globalTrafficCounter = 0
globalLastRed = time.time()

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


# The callback for when the client receives a CONNACK response from the server.
def on_connect_hetzner(client, userdata, flags, rc):
    print("Connected to Local")

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/hetzner/out/#")

def on_connect_htwk(client, userdata, flags, rc):
    print("Connected to HTWK")

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    #client.subscribe("lab/11/")

def turnRed():
    print("turning red")
    client_htwk.publish("lab/11/trafficLight", "red")

def showAnalysis(msg):
    global globalTrafficCounter
    global globalLastRed

    jsonString = json.loads(msg)

    crop_img = pickle.loads(base64.b64decode(codecs.encode(jsonString["crop_img"])))
        
    output_image = draw_bbox(crop_img, jsonString["bbox"], jsonString["label"], jsonString["conf"])


    globalTrafficCounter += len(jsonString["label"])
    client_htwk.publish("lab/11/trafficCounter", str(globalTrafficCounter))

    if len(jsonString["label"]) > 5:
        
        if time.time() - globalLastRed > 20:
            print("turning green")
            globalLastRed = time.time()
            client_htwk.publish("lab/11/trafficLight", "green")
            timer = threading.Timer(5.0, turnRed)
            timer.start()
        else:
            print("too fast, last one was ", time.time() - globalLastRed, "s ago")

    cv2.imshow('frame',output_image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        cap.release()

# The callback for when a PUBLISH message is received from the server.
def on_message_hetzner(client, userdata, msg):
    if msg.topic == "/hetzner/out/analysis":
        #getting analysis picture back
        showAnalysis(msg.payload)

    #client_htwk.publish("lab/08/test", msg.payload)

def on_message_htwk(client, userdata, msg):
    client_hetzner.publish("/toLocal/ok", msg.payload)


def mqtt_client(server, client):
    if server == "local":
        client.on_connect = on_connect_hetzner
        client.on_message = on_message_hetzner
        client.username_pw_set("python", password="gHKwWeCAzEZKGEC7")
        client.connect("94.130.175.62", 1883, 60)
        
    elif server == "htwk":
        client.on_connect = on_connect_htwk
        client.on_message = on_message_htwk
        client.username_pw_set("lab", password="fdit")
        client.connect("mqtt.dit.htwk-leipzig.de", 1883, 60)   
    

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.
    client.loop_forever()
    return



t_loc = threading.Thread(target=mqtt_client, args=("local",client_hetzner), name="MQTT_Local")
t_loc.start()

t_htwk = threading.Thread(target=mqtt_client, args=("htwk",client_htwk), name="MQTT_HTWK")
t_htwk.start()

url = "https://www.youtube.com/watch?v=5_XSYlAfJZM"
video = pafy.new(url)
best = video.getbest(preftype="mp4")
fps = best._info["fps"]

cap = cv2.VideoCapture(best.url)
#height, width, channels = img.shape
#print(height, width, channels)

#cv2.imshow("cropped", frame)
#cv2.waitKey(30)

currFrame = 0

configSecond = 4

while(True):

    #Capture frame-by-frame
    ret, im = cap.read()
    currFrame += 1

    if currFrame % (configSecond * fps) == 0:
    
        cv2.imshow('live',im)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        #cv2.imwrite("test.png", im)

        base64String = codecs.decode(base64.b64encode(zlib.compress(pickle.dumps(im), 9)))

        mqtt_msg = json.dumps({'image': base64String})

        client_hetzner.publish("/hetzner/in/image", mqtt_msg)

  
        #gray = cv2.cvtColor(newframe, cv2.COLOR_BGR2GRAY)
